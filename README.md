ieee802.15.4 test apps for nuttx

instructions

1 - checkout this repository in your nuttx $(APPS) dir (do not make a submodule)

2 - link the export directory as $(APPS)/include/ieee802154

3 - build as usual
